(function(global) {
    // Creates Router
    global.AppRouter = Backbone.Router.extend({
        // Defining routes
        routes: {
            "" : "home",
            "home": "home"
        },

        // Initializing the App
        initialize: function() {
            console.log("Initializing main");

            this.headerView = new HeaderView();
            $('#header').html(this.headerView.render().el);

            console.log("Done initializing main");
        },

        // Home screen
        home: function() {
            console.log("Loading home");

            this.homeView = new HomeView();
            $('#content').html(this.homeView.render().el);

            // Create an empty cart
            var emptyCart = new Cart();
            var item = new CartItem({
                index: 'wahhahah',
                guid: 'abcde',
                title: 'whatever',
                price: 50,
                quantity: 1
            });

            emptyCart.get('items').add(item);

            // Create CartView
            this.cartView = new CartView({ model: emptyCart });
            $('#cart').html(this.cartView.render().el);

            console.log("Done loading home");
        }
    });

    // List of templates to be pre-loaded
    var templates = [ 'header', 'home', 'cart', 'cart-item' ];

    tpl.loadTemplates(templates, function() {
        global.app = new AppRouter();
        Backbone.history.start();
    });
})(window);
