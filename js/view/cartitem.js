window.CartItemView = Backbone.View.extend({
    events: {
        "change .quantity": "render"
    },

    initialize: function() {
        this.template = _.template(tpl.get('cart-item'));
    },

    render:function () {
        this.el = $(this.template(this.model.toJSON()));
        this.delegateEvents();
        return this;
    }
});