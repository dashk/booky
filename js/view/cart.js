window.CartView = Backbone.View.extend({

    initialize: function() {
        this.template = _.template(tpl.get('cart'));
    },

    render:function () {
        console.log('Rendering cart');
        // Renders overall template
        $(this.el).html(this.template(this.model.toJSON()));

        var itemListEl = $(this.el).find('#cart-item-list');

        // Loop through each item in the list
        _.each(this.model.get("items").models, function(item) {
            console.log("Adding " + item.get("index") + " to cart");

            var renderedItem = new CartItemView({ model: item}).render().el;
            itemListEl.append(renderedItem);
        });

        console.log('Done rendering cart');
        return this;
    }
});