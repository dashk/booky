/**
 * Cart Item in Shopping Cart
 *
 * @constructor
 */
window.CartItem = Backbone.Model.extend({
    idAttribute: 'index',
    defaults: {
        index: null,
        guid: null,
        title: '',
        isbn: '',
        price: 0,
        quantity: 0
    },
    total: function() {
        return this.get('price') * this.get('quantity');
    }
});

/**
 * Cart Item collection
 */
window.CartItemCollection = Backbone.Collection.extend({
    model: CartItem,
    initialize: function() {
        this.bind('add', function() {

        });
    },
    total: function() {
        var total = 0;

        this.each(function(item) {
            total += parseInt(item.total());
        });

        return total;
    }
})