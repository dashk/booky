window.Cart = Backbone.Model.extend({
    defaults: function() {
        return {
            total: 100,
            items: new CartItemCollection()
        };
    }
});
