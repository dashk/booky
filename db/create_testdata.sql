SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

-- Book data
INSERT INTO `Book` VALUES('16ee7d21-8762-48e7-a331-3e2f00ea5d97', '1434767957', 'Forgotten God: Reversing Our Tragic Neglect of the Holy Spirit', '8.3 x 6.3 x 0.5 inches', 10.19, 20, 2);
INSERT INTO `Book` VALUES('4f5b8383ae89b', '123', 'Title', 'size', 5, 1, 0);
INSERT INTO `Book` VALUES('67d4172e-321d-4b3a-a78b-a59abb5243ec', '1601422210', 'Radical: Taking Back Your Faith from the American Dream', '5.2 x 0.5 x 8 inches', 8.99, 1, 2);
INSERT INTO `Book` VALUES('a8ae3adc-17a7-44d5-a640-cadd911a986c', '1434768511', 'Crazy Love: Overwhelmed by a Relentless God', '8.2 x 5.6 x 0.6 inches', 9.95, 10, 2);

-- Book type
INSERT INTO `BookType` VALUES(1, 'Bible', 'Bibles');
INSERT INTO `BookType` VALUES(2, 'Non-Bible', 'Non-Bible books');
