<?php

require 'Slim/Slim.php';

$app = new Slim();

// Books
$app->get('/book', 'retrieveBooks');
$app->get('/book/', 'retrieveBooks');
$app->get('/book/:id', 'retrieveBook');
$app->post('/book', 'addBook');
$app->put('/book/:id', 'updateBook');
$app->delete('/book/:id', 'deleteBook');

// Not too RESTful...?
$app->get('/book/search/:query', 'searchBook');

$app->run();

function searchBook($query) {
    global $app;

    $sql = "SELECT id, isbn, title, size, price, stock, type FROM Book WHERE title LIKE :query ORDER BY title";

    try {
        $db = getConnection();

        $stmt = $db->prepare($sql);
        $stmt->execute(array(':query' => '%'.$query.'%'));
        $books = $stmt->fetchAll(PDO::FETCH_OBJ);

        echo json_encode($books);
    }
    catch (PDOException $e) {
        echo getJsonError($e);
    }
    catch (Exception $e) {
        echo getServerError();
    }
}

function deleteBook($id) {
    global $app;

    $sql = "DELETE FROM Book WHERE id = :id";

    try {
        $db = getConnection();

        $stmt = $db->prepare($sql);
        $stmt->bindParam('id', $id);
        $stmt->execute();

        if ($stmt->rowCount() != 1) {
            $app->badRequest();
            return;
        }

        setResponseStatus(200);
    }
    catch (PDOException $e) {
        echo getJsonError($e);
    }
    catch (Exception $e) {
        echo getServerError($e);
    }
}

function updateBook($id) {
    global $app;
    $request = $app->request();
    $book = json_decode($request->getBody());

    if ($book == null) {
        $app->badRequest();
        return;
    }
    else if ($book->id != $id) {
        $app->badRequest();
        return;
    }

    $sql = "UPDATE Book SET isbn = :isbn, title = :title, size = :size, price = :price, stock = :stock, type = :type WHERE id = :id";

    try {
        $db = getConnection();

        $stmt = $db->prepare($sql);
        $stmt->bindParam('id', $id);
        $stmt->bindParam('isbn', $book->isbn);
        $stmt->bindParam('title', $book->title);
        $stmt->bindParam('size', $book->size);
        $stmt->bindParam('stock', $book->stock);
        $stmt->bindParam('type', $book->type);
        $stmt->bindParam('price', $book->price);
        $stmt->execute();

        $db = null;
        echo json_encode($book);
    }
    catch (PDOException $e) {
        echo getJsonError($e);
    }
    catch (Exception $e) {
        echo getServerError($e);
    }
}

function addBook() {
    global $app;
    $request = $app->request();
    $book = json_decode($request->getBody());

    if ($book == null) {
        $app->badRequest();
        return;
    }

    $sql = "INSERT INTO Book (id, isbn, title, size, price, stock, type) VALUES (:id, :isbn, :title, :size, :price, :stock, :type)";

    try {
        $db = getConnection();

        $bookId = uniqid();
        $stmt = $db->prepare($sql);
        $stmt->bindParam('id', $bookId);
        $stmt->bindParam('isbn', $book->isbn);
        $stmt->bindParam('title', $book->title);
        $stmt->bindParam('size', $book->size);
        $stmt->bindParam('stock', $book->stock);
        $stmt->bindParam('type', $book->type);
        $stmt->bindParam('price', $book->price);
        $stmt->execute();

        $db = null;

        $book->id = $bookId;
        echo json_encode($book);
    }
    catch (PDOException $e) {
        echo getJsonError($e);
    }
    catch (Exception $e) {
        echo getServerError($e);
    }
}

function retrieveBooks() {
    $sql = "SELECT id, isbn, title, size, price, stock, type FROM Book ORDER BY title";

    try {
        $db = getConnection();
        $stmt = $db->query($sql);
        $db = null;

        $books = $stmt->fetchAll(PDO::FETCH_OBJ);
        echo json_encode($books);
    }
    catch (PDOException $e) {
        echo getJsonError($e);
    }
    catch (Exception $e) {
        echo getServerError();
    }
}

function retrieveBook($id) {
    global $app;

    $sql = "SELECT id, isbn, title, size, price, stock, type FROM Book WHERE id = :id";

    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("id", $id);
        $stmt->execute();
        $db = null;

        if ($stmt->rowCount() != 1) {
            echo $app->notFound();
            return;
        }

        $book = $stmt->fetchObject();
        echo json_encode($book);
    }
    catch (PDOException $e) {
        echo getJsonError($e);
    }
    catch (Exception $e) {
        echo getServerError($e);
    }
}

function setResponseStatus($statusCode) {
    $app->response()->status($statusCode);
}

function getConnection() {
    $dbhost="localhost";
    $dbuser="root";
    $dbpass="root";
    $dbname="booky";

    $dbh = new PDO("mysql:host=$dbhost;dbname=$dbname", $dbuser, $dbpass);
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    return $dbh;
}

function getServerError($e) {
    return '{"error":{"text":"' . $e->getMessage() . '"}}';
}

function getJsonError($e) {
    return '{"error":{"text":"' . $e->getMessage() . '"}}';
}

?>